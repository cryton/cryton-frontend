export const environment = {
  production: false,
  refreshDelay: 0,
  useHttps: false,
  crytonRESTApiHost: '127.0.0.1',
  crytonRESTApiPort: 8000
};
