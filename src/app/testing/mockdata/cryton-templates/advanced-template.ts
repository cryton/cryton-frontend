/**
 * Expected description of main template dependency graph.
 */
export const advancedTemplateDescription = `plan:
  name: Advanced example
  owner: Test runner
  stages:
    - name: stage-one
      trigger_type: delta
      trigger_args:
        hours: 0
        minutes: 0
        seconds: 5
      steps:
        - name: scan-localhost
          step_type: worker/execute
          arguments:
            module: mod_nmap
            module_arguments:
              target: "{{ target }}"
              ports:
                - 22
          is_init: true
          next:
            - step: bruteforce
              type: result
              value: OK
        - name: bruteforce
          step_type: worker/execute
          arguments:
            module: mod_medusa
            module_arguments:
              target: "{{ target }}"
              credentials:
                username: "{{ username }}"
    - name: stage-two
      trigger_type: HTTPListener
      trigger_args:
        host: localhost
        port: 8082
        routes:
          - path: /index
            method: GET
            parameters:
              - name: a
                value: "1"
      steps:
        - name: ssh-session
          step_type: worker/execute
          arguments:
            module: mod_msf
            module_arguments:
              create_named_session: session_to_target_1
              exploit: auxiliary/scanner/ssh/ssh_login
              exploit_arguments:
                RHOSTS: "{{ target }}"
                USERNAME: $bruteforce.username
                PASSWORD: $bruteforce.password
          is_init: true
          next:
            - step: session-cmd
              type: result
              value: OK
        - name: session-cmd
          step_type: worker/execute
          arguments:
            module: mod_cmd
            module_arguments:
              use_named_session: session_to_target_1
              cmd: "{{ commands.passwd }}"
      depends_on:
        - stage-one
`;
