import { Component, EventEmitter } from '@angular/core';
import { Run } from 'src/app/models/api-responses/run.interface';
import { ExpandedRowInterface } from 'src/app/shared/components/cryton-table/models/expanded-row.interface';

@Component({
  selector: 'app-test',
  template: '<h1>Testing Component</h1>'
})
export class TestComponent implements ExpandedRowInterface<Run> {
  rowData: Run;
  delete = new EventEmitter<void>();
  rowUpdate = new EventEmitter<Run>();

  constructor() {}
}
