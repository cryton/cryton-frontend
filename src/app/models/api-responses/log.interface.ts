export interface Log {
  timestamp: string;
  level: string;
  event: string;
  logger: string;
  error?: string;
  name?: string;
  // run_id?: string;
}
