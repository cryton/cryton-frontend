import { RunState } from '../../types/run-state.type';

export interface StepExecutionReport {
  id: number;
  name: string;
  state: RunState;
  start_time: string;
  finish_time: string;
  output: string;
  serialized_output: Record<string, unknown>;
  evidence_file: string;
  valid: boolean;
}
