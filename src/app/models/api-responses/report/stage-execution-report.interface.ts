import { RunState } from '../../types/run-state.type';
import { RunTimes } from './run-times.interface';
import { StepExecutionReport } from './step-execution-report.interface';

export interface StageExecutionReport extends RunTimes {
  id: number;
  name: string;
  state: RunState;
  output: string;
  serialized_output: Record<string, unknown>;
  step_executions: StepExecutionReport[];
}
