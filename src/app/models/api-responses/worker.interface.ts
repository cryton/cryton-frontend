import { WorkerState } from '../types/worker-state.type';

export interface Worker {
  id: number;
  created_at: string;
  updated_at: string;
  name: string;
  description: string;
  state: WorkerState;
}
