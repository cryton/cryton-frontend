export type RunState =
  | 'PENDING'
  | 'STARTING'
  | 'RUNNING'
  | 'FINISHED'
  | 'IGNORE'
  | 'PAUSING'
  | 'PAUSED'
  | 'ERROR'
  | 'STOPPED'
  | 'STOPPING'
  | 'WAITING'
  | 'AWAITING'
  | 'SCHEDULED';
