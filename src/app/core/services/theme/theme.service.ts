import { OverlayContainer } from '@angular/cdk/overlay';
import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Theme } from 'src/app/modules/template-creator/models/interfaces/theme';
import { tcDarkTheme } from 'src/app/modules/template-creator/styles/themes/template-creator-dark.theme';
import { tcLightTheme } from 'src/app/modules/template-creator/styles/themes/template-creator-light.theme';

interface LocalstorageTheme {
  name: string;
  isDark: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  currentTheme: Theme;
  currentTheme$: Observable<Theme>;

  private _currentTheme$: BehaviorSubject<Theme>;

  constructor(private _overlayContainer: OverlayContainer, @Inject(DOCUMENT) private _document: Document) {
    const savedTheme = JSON.parse(localStorage.getItem('theme')) as LocalstorageTheme;
    if (savedTheme) {
      this._setThemeClass(savedTheme.name);
    }

    const theme = {
      primary: '',
      accent: '',
      warn: '',
      isDark: false,
      templateCreator: tcLightTheme
    };

    this.currentTheme = theme;
    this._currentTheme$ = new BehaviorSubject(theme);
    this.currentTheme$ = this._currentTheme$.asObservable();
    this._emitTheme(savedTheme?.isDark ?? false);
  }

  get primaryColor(): string {
    return getComputedStyle(this._document.body).getPropertyValue('--mat-primary');
  }

  get accentColor(): string {
    return getComputedStyle(this._document.body).getPropertyValue('--mat-accent');
  }

  get warnColor(): string {
    return getComputedStyle(this._document.body).getPropertyValue('--mat-warn');
  }

  changeTheme(name: string, isDark: boolean): void {
    localStorage.setItem('theme', JSON.stringify({ name, isDark }));
    this._setThemeClass(name);
    this._emitTheme(isDark);
  }

  private _setThemeClass(name: string): void {
    const overlayContainerClasses = this._overlayContainer.getContainerElement().classList;
    const documentBodyClasses = this._document.body.classList;

    this._changeClass(overlayContainerClasses, name);
    this._changeClass(documentBodyClasses, name);
  }

  private _emitTheme(isDark: boolean): void {
    const theme = {
      primary: this.primaryColor,
      accent: this.accentColor,
      warn: this.warnColor,
      templateCreator: isDark ? tcDarkTheme : tcLightTheme,
      isDark
    };

    this.currentTheme = theme;
    this._currentTheme$.next(theme);
  }

  private _changeClass(classList: DOMTokenList, newClass: string): void {
    const classesToRemove = Array.from(classList).filter((item: string) => item.includes('-theme'));
    if (classesToRemove.length) {
      classList.remove(...classesToRemove);
    }
    classList.add(newClass);
  }
}
