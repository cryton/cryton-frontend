import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { HttpClientModule } from '@angular/common/http';
import { ChangeDetectionStrategy } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of } from 'rxjs';
import { AlertService } from 'src/app/core/services/alert/alert.service';
import { StepType } from 'src/app/shared/components/cryton-editor/models/step-type.enum';
import { ComponentInputDirective } from 'src/app/shared/directives/component-input.directive';
import { SharedModule } from 'src/app/shared/shared.module';
import { CrytonEditorComponent } from './cryton-editor.component';
import { WorkerCreationStepsComponent } from './steps/worker-creation-steps/worker-creation-steps.component';

describe('CrytonEditorComponent', () => {
  let component: CrytonEditorComponent;
  let fixture: ComponentFixture<CrytonEditorComponent>;
  let loader: HarnessLoader;

  const matSnackBarSpy = jasmine.createSpyObj('MatSnackBar', ['openFromComponent']) as MatSnackBar;
  const alertServiceSpy = jasmine.createSpyObj('alertService', [
    'showError',
    'showSuccess',
    'showWarning'
  ]) as AlertService;

  const getCompletionCell = (): HTMLElement => fixture.nativeElement.querySelector('.overview-item__data-cell-name');

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [CrytonEditorComponent, ComponentInputDirective],
      imports: [
        HttpClientModule,
        BrowserAnimationsModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        MatInputModule,
        MatFormFieldModule
      ],
      providers: [
        { MatSnackBar, useValue: matSnackBarSpy },
        { provide: AlertService, useValue: alertServiceSpy }
      ]
    })
      .overrideComponent(CrytonEditorComponent, { set: { changeDetection: ChangeDetectionStrategy.Default } })
      .compileComponents();
  }));

  beforeEach(async () => {
    fixture = TestBed.createComponent(CrytonEditorComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);

    component = fixture.componentInstance;
    component.stepCount = 1;
    component.itemName = 'Worker';
    component.stepOverviewItems = [{ name: 'Creation Progress', type: StepType.COMPLETION, required: true }];
    component.stepsComponent = WorkerCreationStepsComponent;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be impossible to create a worker with unfinished step', async () => {
    spyOn(component, 'triggerError');
    const progressCell = getCompletionCell();
    const createBtn = await loader.getHarness(MatButtonHarness.with({ text: /.*Create.*/ }));

    expect(progressCell.textContent).toContain('Incomplete');

    await createBtn.click();
    fixture.detectChanges();

    expect(component.triggerError).toHaveBeenCalled();
  });

  it('should swap creation progress to complete on step completion', () => {
    component.stepOverviewData[0].next(true);
    fixture.detectChanges();

    const progressCell = getCompletionCell();
    expect(progressCell.textContent).toContain('Complete');
  });

  it('should display a loading spinner', () => {
    component.creatingSubject$.next(true);
    fixture.detectChanges();

    const loading = fixture.nativeElement.querySelector('.editor__creating');
    expect(loading).toBeTruthy();
  });

  it('should display loading and emit creation event on triggerCreation call', () => {
    spyOn(component.createSubject$, 'next');
    component.stepOverviewData[0].next(true);
    component.triggerCreation();

    expect(component.creatingSubject$.value).toEqual(true);
    expect(component.createSubject$.next).toHaveBeenCalled();
  });

  it('should correctly complete creation', () => {
    spyOn(component, 'eraseProgress');
    spyOn(component.create, 'emit');
    component.completeCreation(of('success'));

    expect(component.creatingSubject$.value).toEqual(false);
    expect(component.eraseProgress).toHaveBeenCalled();
    expect(component.create.emit).toHaveBeenCalled();
  });

  it('should emit erase event on erase button click', async () => {
    spyOn(component.eraseSubject$, 'next');

    const eraseButton = await loader.getHarness(MatButtonHarness.with({ text: /.*Erase.*/ }));
    await eraseButton.click();
    fixture.detectChanges();

    expect(component.eraseSubject$.next).toHaveBeenCalled();
  });
});
