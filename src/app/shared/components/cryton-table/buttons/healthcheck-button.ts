import { Observable, throwError } from 'rxjs';
import { catchError, delay, map, tap } from 'rxjs/operators';
import { WorkersService } from 'src/app/core/http/worker/workers.service';
import { Worker } from 'src/app/models/api-responses/worker.interface';
import { WorkerState } from 'src/app/models/types/worker-state.type';
import { environment } from 'src/environments/environment';
import { ApiActionButton } from './api-action-button';

export class HealthCheckButton extends ApiActionButton<Worker> {
  name = 'Healthcheck';
  icon = 'health_and_safety';

  constructor(private _workersService: WorkersService) {
    super();
  }

  executeAction(worker: Worker): Observable<string> {
    const workerCopy = JSON.parse(JSON.stringify(worker)) as Worker;
    this.addToLoading(worker);

    return this._workersService.healthCheck(worker.id).pipe(
      delay(environment.refreshDelay),
      tap(result => {
        workerCopy.state = this._extractState(result.detail);
        this._rowUpdate$.next(workerCopy);
        this.removeFromLoading(worker);
      }),
      map(result => result.detail),
      catchError(() => {
        this.removeFromLoading(worker);
        return throwError(() => new Error('Healthcheck failed.'));
      })
    );
  }

  private _extractState(detailMessage: string): WorkerState {
    const lastSpaceIndex = detailMessage.lastIndexOf(' ');
    return detailMessage.substring(lastSpaceIndex + 1, detailMessage.length - 1).toUpperCase() as WorkerState;
  }
}
