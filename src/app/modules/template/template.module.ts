import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { CodeEditorModule } from '../code-editor/code-editor.module';
import { ListTemplatesPageComponent } from './pages/list-templates-page/list-templates-page.component';
import { TemplateYamlPageComponent } from './pages/template-yaml-page/template-yaml.component';
import { UploadTemplatePageComponent } from './pages/upload-template-page/upload-template-page.component';
import { TemplateRoutingModule } from './template-routing.module';

@NgModule({
  declarations: [ListTemplatesPageComponent, UploadTemplatePageComponent, TemplateYamlPageComponent],
  imports: [CommonModule, SharedModule, TemplateRoutingModule, CodeEditorModule]
})
export class TemplateModule {}
