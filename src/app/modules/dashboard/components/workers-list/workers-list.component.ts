import { AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { WorkersService } from 'src/app/core/http/worker/workers.service';
import { Worker } from 'src/app/models/api-responses/worker.interface';
import { stagedRenderTrigger } from 'src/app/shared/animations/staged-render.animation';
import { TableDataSource } from 'src/app/shared/components/cryton-table/data-source/table.datasource';
import { WorkersDashboardDataSource } from 'src/app/shared/components/cryton-table/data-source/workers-dahboard.data-source';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-workers-list',
  templateUrl: './workers-list.component.html',
  styleUrls: ['./workers-list.component.scss'],
  animations: [stagedRenderTrigger('.worker')]
})
export class WorkersListComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @Input() dataSource: TableDataSource<Worker> = new WorkersDashboardDataSource(this._workersService);
  pageSize = 4;

  private _destroy$ = new Subject<void>();
  constructor(private _workersService: WorkersService) {}

  ngOnInit(): void {
    this.dataSource.loadItems(0, this.pageSize, null, null);
  }

  ngAfterViewInit(): void {
    this.paginator.page
      .pipe(
        tap(() => this.loadWorkers()),
        takeUntil(this._destroy$)
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  loadWorkers(): void {
    const offset = this.paginator ? this.paginator.pageIndex * this.paginator.pageSize : 0;
    this.dataSource.loadItems(offset, this.pageSize, null, null);
  }

  /**
   * Refreshes table data with a small time-out to simulate loading data even if data gets
   * loaded almost instantly.
   */
  refreshData(): void {
    this.dataSource.setLoading(true);

    setTimeout(() => {
      this.loadWorkers();
    }, environment.refreshDelay);
  }
}
