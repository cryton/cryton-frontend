import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { MSFForm } from '../../../classes/stage-creation/forms/msf-trigger-form';
import { TriggerParameters } from '../../../classes/stage-creation/trigger-parameters';
import { ERROR_MESSAGES } from './msf-trigger.errors';

@Component({
  selector: 'app-msf-trigger-parameters',
  templateUrl: './msf-trigger-parameters.component.html',
  styleUrls: ['./msf-trigger-parameters.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MsfTriggerParametersComponent extends TriggerParameters {
  @Input() triggerForm: MSFForm;

  constructor() {
    super(ERROR_MESSAGES);
  }
}
