import { ChangeDetectionStrategy } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ImmediateForm } from '../../../classes/stage-creation/forms/immediate-form';
import { ImmediateTriggerParametersComponent } from './immediate-trigger-parameters.component';

describe('DeltaTriggerParametersComponent', () => {
  let component: ImmediateTriggerParametersComponent;
  let fixture: ComponentFixture<ImmediateTriggerParametersComponent>;

  const testTriggerForm = new ImmediateForm();

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatFormFieldModule, FormsModule, ReactiveFormsModule, MatInputModule, BrowserAnimationsModule],
      declarations: [ImmediateTriggerParametersComponent]
    })
      .overrideComponent(ImmediateTriggerParametersComponent, { set: { changeDetection: ChangeDetectionStrategy.Default } })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImmediateTriggerParametersComponent);
    component = fixture.componentInstance;
    component.triggerForm = testTriggerForm;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
