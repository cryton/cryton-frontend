import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ImmediateForm } from '../../../classes/stage-creation/forms/immediate-form';
import { TriggerParameters } from '../../../classes/stage-creation/trigger-parameters';
import { ERROR_MESSAGES } from './immediate-trigger.errors';

@Component({
  selector: 'app-immediate-trigger-parameters',
  templateUrl: './immediate-trigger-parameters.component.html',
  styleUrls: ['./immediate-trigger-parameters.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImmediateTriggerParametersComponent extends TriggerParameters {
  @Input() triggerForm: ImmediateForm;

  constructor() {
    super(ERROR_MESSAGES);
  }
}
