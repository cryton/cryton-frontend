import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { debounceTime, filter, takeUntil } from 'rxjs/operators';
import { AlertService } from 'src/app/core/services/alert/alert.service';
import { ComponentInputDirective } from 'src/app/shared/directives/component-input.directive';
import { DependencyGraph } from '../../classes/dependency-graph/dependency-graph';
import { StepArguments, StepNode } from '../../classes/dependency-graph/node/step-node';
import { yamlValidator } from '../../classes/utils/validate-yaml';
import { parse, stringify } from 'yaml';
import { CreateStageRoute } from '../../models/enums/tc-routes.enum';
import { StepCreatorHelpComponent } from '../../pages/help-pages/step-creator-help/step-creator-help.component';
import { DependencyGraphManagerService, DepGraphRef } from '../../services/dependency-graph-manager.service';
import { TcRoutingService } from '../../services/tc-routing.service';
import { TemplateCreatorStateService } from '../../services/template-creator-state.service';

export const CREATION_MSG_TIMEOUT = 7000;

@Component({
  selector: 'app-step-creator',
  templateUrl: './step-creator.component.html',
  styleUrls: ['./step-creator.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StepCreatorComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(ComponentInputDirective, { static: true }) typeArgumentsContainer: ComponentInputDirective;

  stepForm = new FormGroup({
    name: new FormControl<string>(null, [Validators.required]),
    metadata: new FormControl<string>(null, {
      validators: [yamlValidator()],
      updateOn: 'blur'
    }),
    module: new FormControl<string>(null, [Validators.required]),
    arguments: new FormControl<string>(null, {
      validators: [Validators.required, yamlValidator()],
      updateOn: 'blur'
    }),
    output_alias: new FormControl<string>(null, [Validators.pattern(/^[^\$\.]*$/)]),
    output_mapping: new FormArray<FormGroup<{ from: FormControl<string>; to: FormControl<string> }>>([]),
    output_replace: new FormArray<FormGroup<{ what: FormControl<string>; with: FormControl<string> }>>([])
  });

  editedStep: StepNode;
  showCreationMessage$: Observable<boolean>;

  private _depGraph: DependencyGraph;
  private _destroy$ = new Subject<void>();
  private _stepArgsBackup: StepArguments;
  private _showCreationMessage$ = new BehaviorSubject<boolean>(false);

  constructor(
    private _graphManager: DependencyGraphManagerService,
    private _alertService: AlertService,
    private _dialog: MatDialog,
    private _tcRouter: TcRoutingService,
    private _cd: ChangeDetectorRef,
    private _tcState: TemplateCreatorStateService
  ) {
    this.showCreationMessage$ = this._showCreationMessage$.asObservable();
  }

  get stepArguments(): Omit<StepArguments, 'theme'> {
    const { output_mapping, output_alias, output_replace, ...rest } = this.stepForm.getRawValue();
    const mapping = output_mapping.map(m => ({ from: m.from, to: m.to }));
    const replace = Object.fromEntries(output_replace.map(m => [m.what, m.with]));
    return { output: { mapping: mapping, replace: replace, alias: output_alias }, ...rest };
  }

  ngOnInit(): void {
    this._createDepGraphSub();
    this._createCreationMsgSub();

    if (!this.editedStep) {
      this.restoreStepForm();
    }
  }

  ngAfterViewInit(): void {
    this._createEditNodeSub();
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  /**
   * Opens help page.
   */
  openHelp(): void {
    this._dialog.open(StepCreatorHelpComponent, { width: '60%', maxWidth: '80rem' });
  }

  /**
   * Creates step and resets step form.
   */
  handleCreateStep(): void {
    if (!this.stepForm.valid) {
      this._alertService.showError('Step form is invalid.');
    } else {
      const step: StepNode = this._createStep();
      this.reset();
      this._alertService.showSuccess('Step created successfully');
      this._graphManager.addDispenserNode(DepGraphRef.STAGE_CREATION, step);
      this._showCreationMessage$.next(true);
    }
  }

  /**
   * Resets form and edited step.
   */
  cancelEditing(): void {
    this.editedStep = null;

    if (!this.restoreStepForm()) {
      this.reset();
    }
  }

  /**
   * Cancels editing and switches tab back to create stage tab.
   */
  handleCancelClick(): void {
    this._depGraph.clearEditNode();
  }

  /**
   * Saves changes to currently edited step and cancels editing.
   */
  handleSaveChanges(): void {
    this.editedStep.edit(this.stepArguments);
    this._depGraph.clearEditNode();

    // Needed to update displayed arguments in dispenser.
    this._graphManager.refreshDispenser(DepGraphRef.STAGE_CREATION);

    // If we edit step that is a part of stage that is also being edited
    // we need to navigate the user back to stage creation page.
    // This is because the stage needs to be also saved for the changes
    // to be reflected in the final template.
    if (this._tcState.editedStage) {
      this._tcRouter.navigateTo(2, CreateStageRoute.STAGE_PARAMS);
    } else {
      // Else navigate user back to the dependency graph where he started
      // the step editing.
      this._tcRouter.navigateTo(2, CreateStageRoute.DEP_GRAPH);
    }
  }

  /**
   * Gets errors for the create step button's tooltip.
   *
   * @returns Error string.
   */
  getTooltipErrors(): string {
    if (!this.stepForm.valid) {
      return '- Invalid step arguments.';
    }
  }

  /**
   * Backs up step form.
   */
  backupStepForm(): void {
    this._stepArgsBackup = this.stepArguments;
  }

  /**
   * Restores step form from backup, returns true if there was a backed up form value.
   */
  restoreStepForm(): boolean {
    if (this._stepArgsBackup) {
      this.fillFormWithArgs(this._stepArgsBackup);
      this._stepArgsBackup = null;
      return true;
    }
    return false;
  }

  navigateToStagesDepGraph(): void {
    this._tcRouter.navigateTo(2, CreateStageRoute.DEP_GRAPH);
  }

  /**
   * Fills out form fields with step arguments.
   *
   * @param step Step to fill arguments of.
   */
  fillFormWithStep(step: StepNode): void {
    this.fillFormWithArgs(step.args);
  }

  isValid(): boolean {
    return this.stepForm.valid;
  }

  addMapping(from?: string, to?: string): void {
    const mappingArray = this.stepForm.get('output_mapping') as FormArray;

    mappingArray.insert(
      0,
      new FormGroup({
        from: new FormControl(from ?? null, Validators.required),
        to: new FormControl(to ?? null, Validators.required)
      })
    );
  }

  removeMapping(index: number): void {
    this.stepForm.controls.output_mapping.removeAt(index);
  }

  addReplace(what?: string, with_?: string): void {
    const replaceArray = this.stepForm.get('output_replace') as FormArray;

    replaceArray.insert(
      0,
      new FormGroup({
        what: new FormControl(what ?? null, Validators.required),
        with: new FormControl(with_ ?? null, Validators.required)
      })
    );
  }

  removeReplace(index: number): void {
    this.stepForm.controls.output_replace.removeAt(index);
  }

  reset(): void {
    this.stepForm.reset({ name: '', metadata: '', module: '', arguments: '', output_alias: '' });
    (this.stepForm.get('output_mapping') as FormArray).clear();
    (this.stepForm.get('output_replace') as FormArray).clear();
  }

  fillFormWithArgs(args: StepArguments): void {
    const { output, ...form_arguments } = args;

    this.reset();
    this.stepForm.patchValue({
      // arguments: module_arguments ? stringify(module_arguments) : undefined,
      ...form_arguments
    });
    this.stepForm.get('output_alias').setValue(output.alias);
    output.mapping?.forEach(mapping => this.addMapping(mapping.from, mapping.to));
    Object.entries(output.replace).forEach(([key, value]) => this.addReplace(key, value));
  }

  /**
   * Initializes editing of the step passed in the argument.
   *
   * @param step Step to edit.
   */
  private _startEditing(step: StepNode): void {
    if (step !== this.editedStep) {
      if (!this.editedStep) {
        this.backupStepForm();
      }
      this.fillFormWithStep(step);
      this.editedStep = step;
    }
  }

  /**
   * Creates new cryton step from arguments in step form.
   *
   * @returns Cryton step.
   */
  private _createStep(): StepNode {
    return new StepNode(this.stepForm.get('name').value, this.stepArguments);
  }

  /**
   * Subscribes to stage creation dep. graph subject in manager.
   *
   * On every next():
   * - Saves current dep. graph and node manager to a local variable.
   * - Initializes unique name validator.
   */
  private _createDepGraphSub(): void {
    this._graphManager
      .getCurrentGraph(DepGraphRef.STAGE_CREATION)
      .pipe(takeUntil(this._destroy$))
      .subscribe(depGraph => {
        this._depGraph = depGraph;
      });
  }

  private _createEditNodeSub(): void {
    this._graphManager
      .observeNodeEdit(DepGraphRef.STAGE_CREATION)
      .pipe(takeUntil(this._destroy$))
      .subscribe((step: StepNode) => {
        if (step) {
          this._startEditing(step);
        } else if (this.editedStep) {
          this.cancelEditing();
        }
      });
  }

  private _createCreationMsgSub(): void {
    this._showCreationMessage$
      .pipe(
        takeUntil(this._destroy$),
        filter(val => val),
        debounceTime(CREATION_MSG_TIMEOUT)
      )
      .subscribe(() => this._showCreationMessage$.next(false));
  }
}
