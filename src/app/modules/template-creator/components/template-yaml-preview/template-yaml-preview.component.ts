import { ChangeDetectionStrategy, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AlertService } from 'src/app/core/services/alert/alert.service';
import { CodeEditorTextareaComponent } from 'src/app/modules/code-editor/components/code-editor-textarea/code-editor-textarea.component';
import { parse } from 'yaml';
import { PlanDescription } from '../../models/interfaces/template-description';
import { InvalidTemplateFormatError } from './errors/invalid-template-format.error';
import { UndefinedTemplatePropertyError } from './errors/undefined-template-property.error';

@Component({
  selector: 'app-template-yaml-preview',
  templateUrl: './template-yaml-preview.component.html',
  styleUrls: ['./template-yaml-preview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TemplateYamlPreviewComponent implements OnInit {
  @ViewChild(CodeEditorTextareaComponent) codeEditor!: CodeEditorTextareaComponent;

  readonly form = new FormGroup({
    template: new FormControl('', [Validators.required])
  });

  get templateControl(): FormControl<string> {
    return this.form.controls.template;
  }

  constructor(
    private _dialogRef: MatDialogRef<TemplateYamlPreviewComponent>,
    private _alert: AlertService,
    @Inject(MAT_DIALOG_DATA) private _data: { template: string }
  ) {}

  ngOnInit(): void {
    this.form.get('template').setValue(this._data.template);
  }

  handleCreate(): void {
    const templateControlVal = this.templateControl.value;

    try {
      if (!templateControlVal) {
        throw new InvalidTemplateFormatError();
      }
      const template = this._tryParsingTemplate(this.templateControl.value);
      this._checkTemplateValues(template);
      this._dialogRef.close(this.templateControl.value);
    } catch (e) {
      if (e instanceof Error) {
        this._alert.showError(e.message);
      }
    }
  }

  private _tryParsingTemplate(templateYaml: string): PlanDescription {
    try {
      return parse(templateYaml) as PlanDescription;
    } catch (e) {
      console.error(e);
      throw new InvalidTemplateFormatError();
    }
  }

  private _checkTemplateValues(template: PlanDescription): void {
    if (!template || !template) {
      throw new InvalidTemplateFormatError();
    } else if (!template.name || template.name === '') {
      throw new UndefinedTemplatePropertyError('name');
    }
  }
}
