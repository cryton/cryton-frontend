import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TemplateCreatorPageComponent } from './pages/template-creator-page/template-creator-page.component';

const routes: Routes = [
  {
    path: '',
    component: TemplateCreatorPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TemplateCreatorRoutingModule {}
