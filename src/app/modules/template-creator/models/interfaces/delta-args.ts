/**
 * Delta stage trigger parameters.
 */
export interface DeltaArgs {
  days: number;
  hours: number;
  minutes: number;
  seconds: number;
}
