export interface MSFArgs {
  module_name?: string;
  timeout?: number;
  datastore?: object;
  commands?: string[];
}
