import { TriggerArgs } from '../../classes/triggers/trigger';
import { TriggerType } from '../enums/trigger-type';

/**
 * Plan detail structure.
 */
export interface PlanDescription {
  name: string;
  metadata: object;
  stages: { [id: string]: StageDescription };
}

/**
 * Plan template stage.
 */
export interface StageDescription {
  depends_on?: string[];
  metadata?: object;
  arguments?: TriggerArgs;
  type?: TriggerType;
  steps: { [id: string]: StepDescription };
}

export interface OutputMapping {
  from: string;
  to: string;
}

export interface OutputArguments {
  alias?: string;
  mapping?: OutputMapping[];
  replace?: object;
}

/**
 * Step in stage.
 */
export interface StepDescription {
  is_init?: boolean;
  metadata?: string;
  module: string;
  arguments: string;
  next?: StepEdgeDescription[];
  output?: OutputArguments;
}

/**
 * Edge between steps.
 */
export interface StepEdgeDescription {
  type: string;
  value?: string;
  step: string;
}
