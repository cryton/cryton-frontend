export enum TriggerType {
  IMMEDIATE = 'immediate',
  DELTA = 'delta',
  TIME = 'time',
  HTTP = 'http',
  METASPLOIT = 'metasploit'
}
