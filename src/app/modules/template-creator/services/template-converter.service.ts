import { Injectable } from '@angular/core';
import { parse, stringify } from 'yaml';
import { StageNode } from '../classes/dependency-graph/node/stage-node';
import { PlanDescription } from '../models/interfaces/template-description';
import { DependencyGraphManagerService, DepGraphRef } from './dependency-graph-manager.service';
import { TemplateCreatorStateService } from './template-creator-state.service';

@Injectable({
  providedIn: 'root'
})
export class TemplateConverterService {
  constructor(private _state: TemplateCreatorStateService, private _graphManager: DependencyGraphManagerService) {}

  /**
   * Exports template from the template creator to the YAML description.
   *
   * @returns YAML description of the template.
   */
  exportYAMLTemplate(): string {
    const templateDepGraph = this._graphManager.getCurrentGraph(DepGraphRef.TEMPLATE_CREATION).value;
    const name = this._state.templateForm.get('name').value as string;
    const metadata = this._state.templateForm.get('metadata').value
      ? parse(this._state.templateForm.get('metadata').value)
      : undefined;

    const template: PlanDescription = { name, metadata, stages: {} };

    templateDepGraph.nodes.forEach((node: StageNode) => {
      const [stageName, stageDescription] = node.getYaml();

      if (node.parentEdges.length > 0) {
        stageDescription.depends_on = [];
        node.parentEdges.forEach(edge => {
          stageDescription.depends_on.push(edge.parentNode.name);
        });
      }

      template.stages[stageName] = stageDescription;
    });

    return stringify(template);
  }
}
