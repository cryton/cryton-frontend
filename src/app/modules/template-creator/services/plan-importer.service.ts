import { Injectable } from '@angular/core';
import { ThemeService } from 'src/app/core/services/theme/theme.service';
import { withoutUndefinedAndNull } from 'src/app/shared/utils/without-undefined';
import { DependencyGraph } from '../classes/dependency-graph/dependency-graph';
import { StepEdge } from '../classes/dependency-graph/edge/step-edge';
import { CompositeKonvaGraphNode } from '../classes/dependency-graph/node/composite-konva-graph-node';
import { RegularKonvaGraphNode } from '../classes/dependency-graph/node/regular-konva-graph-node';
import { StageNode } from '../classes/dependency-graph/node/stage-node';
import { StepArguments, StepNode } from '../classes/dependency-graph/node/step-node';
import { TriggerFactory } from '../classes/triggers/trigger-factory';
import {
  PlanDescription,
  StageDescription,
  StepDescription,
  StepEdgeDescription
} from '../models/interfaces/template-description';

@Injectable({
  providedIn: 'root'
})
export class PlanImporterService {
  constructor(private _themeService: ThemeService) {}

  /**
   * Imports plan into the dependency graph.
   *
   * @param depGraph Dependency graph into which the plan should be imported.
   * @param plan Plan description as JSON object.
   */
  importPlan(depGraph: DependencyGraph, plan: PlanDescription): void {
    const stagesWithParents: Record<string, { stage: StageNode; parents: string[] }> = {};

    Object.entries(plan.stages).forEach(([stageName, stageDescription]) => {
      const stage = this._createStage(stageName, stageDescription);
      depGraph.addNode(stage);
      stagesWithParents[stageName] = {
        stage,
        parents: stageDescription.depends_on
      };
    });

    this._createStageEdges(stagesWithParents, depGraph);
  }

  /**
   * Creates cryton stage from the YAML description.
   *
   * @param name
   * @param stageDescription YAML description of the stage.
   * @returns Cryton stage.
   */
  private _createStage(name: string, stageDescription: StageDescription): StageNode {
    const childDepGraph = new DependencyGraph(this._themeService.currentTheme);
    const stepsWithEdges: Record<string, { step: StepNode; next: StepEdgeDescription[] }> = {};

    Object.entries(stageDescription.steps).forEach(([stepName, stepDescription]) => {
      const step = this._createStep(stepName, stepDescription);
      childDepGraph.addNode(step);
      stepsWithEdges[stepName] = { step, next: stepDescription.next };
    });
    this._createStepEdges(stepsWithEdges, childDepGraph);

    const trigger = TriggerFactory.createTrigger(stageDescription.type, stageDescription.arguments);

    const crytonStage = new StageNode(name, {
      metadata: stageDescription.metadata,
      childDepGraph,
      trigger
    });

    crytonStage.konvaGraphNode = new CompositeKonvaGraphNode(
      this._themeService.currentTheme,
      name,
      stageDescription.type,
      childDepGraph
    );

    return crytonStage;
  }

  /**
   * Creates all edges between steps.
   *
   * @param steps Record with step name as a key and an object with
   * cryton step and YAML representations of all of its edges as the value.
   * @param depGraph
   */
  private _createStepEdges(
    steps: Record<string, { step: StepNode; next: StepEdgeDescription[] }>,
    depGraph: DependencyGraph
  ): void {
    Object.values(steps).forEach(stepWithEdges => {
      // <Parent step name, step's edges>
      const createdEdges: Record<string, StepEdge[]> = {};

      stepWithEdges.next?.forEach(edgeYaml => {
        let matchingEdge: StepEdge;

        if (!createdEdges[stepWithEdges.step.name]) {
          matchingEdge = this._createStepEdge(stepWithEdges.step, steps[edgeYaml.step].step, depGraph);
          createdEdges[stepWithEdges.step.name] = [matchingEdge];
        } else {
          const edgeToChild = createdEdges[stepWithEdges.step.name].find(
            edge => edge.childNode.name === steps[edgeYaml.step].step.name
          );

          if (edgeToChild) {
            matchingEdge = edgeToChild;
          } else {
            matchingEdge = this._createStepEdge(stepWithEdges.step, steps[edgeYaml.step].step, depGraph);
            createdEdges[stepWithEdges.step.name].push(matchingEdge);
          }
        }
        matchingEdge.conditions.push({ type: edgeYaml.type, value: edgeYaml.value });
      });
    });
  }

  /**
   * Creates a single edge between two nodes.
   *
   * @param parentNode Parent node.
   * @param childNode Child node.
   * @param depGraph
   * @returns Cryton step edge.
   */
  private _createStepEdge(parentNode: StepNode, childNode: StepNode, depGraph: DependencyGraph): StepEdge {
    return depGraph.addEdge(parentNode, childNode) as StepEdge;
  }

  /**
   * Creates all edges between stages.
   *
   * @param stages Record with stage name as a key and an object with cryton stage and names of all of its parents as the value.
   * @param depGraph
   */
  private _createStageEdges(
    stages: Record<string, { stage: StageNode; parents: string[] }>,
    depGraph: DependencyGraph
  ) {
    Object.values(stages).forEach(stageWithParent => {
      stageWithParent.parents?.forEach(parent => {
        depGraph.addEdge(stages[parent].stage, stageWithParent.stage);
      });
    });
  }

  /**
   * Creates step from the YAML representation.
   *
   * @param name
   * @param stepDescription YAML description of the step.
   * @returns Cryton step.
   */
  private _createStep(name: string, stepDescription: StepDescription): StepNode {
    const step = new StepNode(
      name,
      withoutUndefinedAndNull({
        metadata: stepDescription.metadata,
        module: stepDescription.module,
        arguments: stepDescription.arguments,
        output: stepDescription.output
      }) as StepArguments
    );

    step.konvaGraphNode = new RegularKonvaGraphNode(this._themeService.currentTheme, name, stepDescription.metadata);

    return step;
  }
}
