import { Injectable } from '@angular/core';
// import { withoutUndefinedAndNull } from 'src/app/shared/utils/without-undefined';
// import { DependencyGraph } from '../classes/dependency-graph/dependency-graph';
// import { StageGraphNode } from '../classes/dependency-graph/node/stage-graph-node';
// import { StepArguments, StepNode } from '../classes/dependency-graph/node/step-node';
// import { TriggerFactory } from '../classes/triggers/trigger-factory';
// import {
//   PlanDescription,
//   StageDescription,
//   StepDescription,
//   StepEdgeDescription
// } from '../models/interfaces/template-description';

@Injectable({
  providedIn: 'root'
})
export class PlanOverviewImporterService {
  constructor() {}

  // importPlan(template: PlanDescription, depGraph: DependencyGraph) {
  //   const stagesWithParents: Record<string, { stage: StageGraphNode; parents: string[] }> = {};
  //   Object.entries(template.stages).forEach(([stageName, stageDescription]) => {
  //     const stage = this._createStageGraph(stageName, stageDescription);
  //     depGraph.addNode(stage);
  //     stagesWithParents[stageName] = {
  //       stage,
  //       parents: stageDescription.depends_on
  //     };
  //   });
  //
  //   // this._createStageEdges(stagesWithParents);
  // }
  //
  // private _createStageGraph(stageName: string, stageDescription: StageDescription): StageGraphNode {
  //   const depGraph = new DependencyGraph();
  //
  //   const stepsWithEdges: Record<string, { step: StepNode; next: StepEdgeDescription[] }> = {};
  //
  //   Object.entries(stageDescription.steps).forEach(([stepName, stepDescription]) => {
  //     const step = this._createStep(stepName, stepDescription, depGraph);
  //     depGraph.addNode(step);
  //     stepsWithEdges[stepName] = { step, next: stepDescription.next };
  //   });
  //
  //   const trigger = TriggerFactory.createTrigger(stageDescription.type, stageDescription.arguments);
  //
  //   return new StageGraphNode({ name: stageName, trigger, childDepGraph: depGraph });
  // }
  //
  // /**
  //  * Creates step from the YAML representation.
  //  *
  //  * @param name
  //  * @param stepDescription YAML description of the step.
  //  * @param parentDepGraph Step's parent dependency graph.
  //  * @returns Cryton step.
  //  */
  // private _createStep(name: string, stepDescription: StepDescription, parentDepGraph: DependencyGraph): StepNode {
  //   const step = new StepNode(
  //     name,
  //     withoutUndefinedAndNull({
  //       module: stepDescription.module,
  //       arguments: stepDescription.arguments,
  //       output: {
  //         alias: stepDescription.output.alias,
  //         mapping: stepDescription.output.mapping,
  //         replace: stepDescription.output.replace
  //       }
  //     }) as StepArguments
  //   );
  //   step.setParentDepGraph(parentDepGraph);
  //
  //   return step;
  // }
}
