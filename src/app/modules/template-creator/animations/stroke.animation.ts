import Konva from 'konva';
import { LineCap } from 'konva/lib/Shape';

export class StrokeAnimation {
  private _strokedShape: Konva.Shape;
  private _listenerNode: Konva.Node;

  private _defaultColor: string;
  private _defaultStrokeWidth: number;
  private _defaultLineCap: LineCap;
  private _defaultFill: string;

  private _animation: Konva.Animation;

  constructor(strokedShape: Konva.Shape, listenerNode: Konva.Node) {
    this._strokedShape = strokedShape;
    this._listenerNode = listenerNode;
  }

  /**
   * Activates stroke animation.
   *
   * @param color Color of the stroke.
   * @param deactivateOnMouseLeave Will automatically deactivate on mouseleave event if true.
   */
  activate(color: string, deactivateOnMouseLeave = true): void {
    this._saveDefaults();
    this._strokedShape.strokeEnabled(true);
    this._strokedShape.stroke(color);
    this._strokedShape.strokeWidth(3);
    this._strokedShape.lineCap('round');
    this._strokedShape.dash([12, 10]);

    if (this._strokedShape instanceof Konva.Arrow) {
      this._strokedShape.fill(color);
    }

    this._animation = new Konva.Animation(frame => {
      this._strokedShape.dashOffset(Math.round(frame.time / 20));
    });

    this._animation.start();

    if (deactivateOnMouseLeave) {
      this._listenerNode.on('mouseleave', () => {
        this.deactivate();
      });
    }
  }

  /**
   * Deactivates stroke animation.
   */
  deactivate(): void {
    if (this._strokedShape instanceof Konva.Arrow) {
      this._strokedShape.stroke(this._defaultColor);
      this._strokedShape.strokeWidth(this._defaultStrokeWidth);
      this._strokedShape.lineCap(this._defaultLineCap);
      this._strokedShape.fill(this._defaultFill);
      this._strokedShape.dash([]);
    } else {
      this._strokedShape.strokeEnabled(false);
    }

    this._animation?.stop();
  }

  /**
   * Saves default stroke and color settings before activating animation.
   */
  private _saveDefaults(): void {
    this._defaultColor = this._strokedShape.stroke();
    this._defaultStrokeWidth = this._strokedShape.strokeWidth();
    this._defaultLineCap = this._strokedShape.lineCap();
    this._defaultFill = this._strokedShape.fill();
  }
}
