import { parse } from 'yaml';
import { OutputArguments, StepDescription } from '../../../models/interfaces/template-description';
import { GraphNode } from './graph-node';

export interface StepArguments {
  metadata?: string;
  module: string;
  arguments: string;
  output?: OutputArguments;
}

export class StepNode extends GraphNode {
  constructor(public name_: string, public args: StepArguments) {
    super(name_, args.metadata);
  }

  /**
   * Edits step args.
   *
   * @param args New step args.
   */
  edit(args: StepArguments): void {
    this.name = this.name_;
    this.args = args;
  }

  copy(): StepNode {
    const argsCopy = Object.assign({}, this.args);
    const nodeCopy = new StepNode(this.name_, argsCopy);

    if (this.konvaGraphNode) {
      nodeCopy.konvaGraphNode = this.konvaGraphNode.copy();
    }

    return nodeCopy;
  }

  getYaml(): [string, StepDescription] {
    return [
      this.name,
      {
        metadata: this.args.metadata ? parse(this.args.metadata) : undefined,
        module: this.args.module,
        arguments: this.args.arguments ? parse(this.args.arguments) : undefined,
        output: this.args.output
      }
    ];
  }
}
