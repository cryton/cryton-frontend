import { withoutUndefinedAndNull } from 'src/app/shared/utils/without-undefined';
import {
  StageDescription,
  StepDescription,
  StepEdgeDescription
} from '../../../models/interfaces/template-description';
import { Trigger, TriggerArgs } from '../../triggers/trigger';
import { DependencyGraph } from '../dependency-graph';
import { StepEdge } from '../edge/step-edge';
import { GraphNode } from './graph-node';
import { StepNode } from './step-node';

export interface CrytonStageArguments {
  metadata: object;
  childDepGraph: DependencyGraph;
  trigger: Trigger<TriggerArgs>;
}

export class StageNode extends GraphNode {
  public metadata: object;

  constructor(public name_: string, public args: CrytonStageArguments) {
    super(name_, args.trigger.getType());
    this.metadata = args.metadata;
  }

  get trigger(): Trigger<TriggerArgs> {
    return this.args.trigger;
  }

  get childDepGraph(): DependencyGraph {
    return this.args.childDepGraph;
  }

  /**
   * Edits child dependency graph.
   *
   * @param childDepGraph New child dependency graph.
   */
  editChildDepGraph(childDepGraph: DependencyGraph): void {
    this.args.childDepGraph = childDepGraph;
  }

  /**
   * Edits stage trigger.
   *
   * @param trigger Stage trigger.
   */
  editTrigger(trigger: Trigger<TriggerArgs>): void {
    this.args.trigger = trigger;
  }

  getYaml(): [string, StageDescription] {
    return [
      this.name,
      {
        metadata: this.metadata,
        type: this.trigger.getType(),
        arguments: this.trigger.getArgs(),
        steps: this._createStepsYaml(this.childDepGraph.nodes as StepNode[])
      }
    ];
  }

  copy(): StageNode {
    const argsCopy = Object.assign({}, this.args);
    const nodeCopy = new StageNode(this.name_, argsCopy);

    if (this.konvaGraphNode) {
      nodeCopy.konvaGraphNode = this.konvaGraphNode.copy();
    }

    return nodeCopy;
  }

  /**
   * Creates object from the steps which can be easily stringified into YAML representation.
   *
   * @param steps Cryton steps.
   * @returns Steps yaml object.
   */
  private _createStepsYaml(steps: StepNode[]): { [id: string]: StepDescription } {
    const parsedSteps: { [id: string]: StepDescription } = {};

    steps.forEach(step => {
      let [stepName, stepDescription] = step.getYaml();
      const next: StepEdgeDescription[] = [];

      step.childEdges.forEach((edge: StepEdge) => {
        edge.conditions.forEach(condition => {
          next.push({ step: edge.childNode.name, ...condition });
        });
      });

      if (step.parentEdges.length === 0) {
        stepDescription.is_init = true;
      }
      if (next.length > 0) {
        stepDescription.next = next;
      }

      stepDescription = withoutUndefinedAndNull(stepDescription) as StepDescription;
      parsedSteps[stepName] = stepDescription;
    });

    return parsedSteps;
  }
}
