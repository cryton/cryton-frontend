import Konva from 'konva';
import { EventConfigurer } from '../event-configurer';
import { GraphNode } from './graph-node';

export interface CompositeNodeEventConfigurer extends EventConfigurer {
  configure(
    node: GraphNode,
    primaryLabel: Konva.Label,
    secondaryLabel: Konva.Label,
    borderRect: Konva.Rect,
    topLine: Konva.Line,
    bottomLine: Konva.Line
  ): void;

  removeOwnListeners(
    node: GraphNode,
    primaryLabel: Konva.Label,
    secondaryLabel: Konva.Label,
    borderRect: Konva.Rect,
    topLine: Konva.Line,
    bottomLine: Konva.Line
  ): void;

  removeAllListeners(
    node: GraphNode,
    primaryLabel: Konva.Label,
    secondaryLabel: Konva.Label,
    borderRect: Konva.Rect,
    topLine: Konva.Line,
    bottomLine: Konva.Line
  ): void;
}
