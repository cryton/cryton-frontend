import Konva from 'konva';
import { Cursor } from '../cursor-state';
import { DependencyGraphEditor } from '../dependency-graph-editor';
import { GraphEdge } from './graph-edge';
import { RegularEdgeEventConfigurer } from './regular-edge-event-configurer';
import { RegularKonvaGraphEdge } from './regular-konva-graph-edge';

export class EditorStageEdgeEventConfigurer implements RegularEdgeEventConfigurer {
  constructor(
    private _editor: DependencyGraphEditor,
    private _edge: GraphEdge,
    private _konvaGraphEdge: RegularKonvaGraphEdge
  ) {}

  configure(arrow: Konva.Arrow): void {
    this.removeOwnListeners(arrow);

    arrow.on('mouseenter.editor-stage-edge', () => {
      this._onMouseEnter();
    });
    arrow.on('mouseleave.editor-stage-edge', () => {
      this._onMouseLeave();
    });
    arrow.on('click.editor-stage-edge', () => {
      this._onClick();
    });
  }

  removeOwnListeners(arrow: Konva.Arrow): void {
    arrow.off('mouseenter.editor-stage-edge');
    arrow.off('mouseleave.editor-stage-edge');
    arrow.off('click.editor-stage-edge');
  }

  removeAllListeners(arrow: Konva.Arrow): void {
    arrow.off();
  }

  private _onMouseEnter(): void {
    if (this._isDeleteEnabled()) {
      this._editor.cursorState.setCursor(Cursor.POINTER);
      this._konvaGraphEdge.activateStroke(this._editor.theme.primary, true);
    }
  }

  private _onMouseLeave(): void {
    this._editor.cursorState.unsetCursor(Cursor.POINTER);
    this._konvaGraphEdge.setTheme(this._editor.theme);
  }

  private _onClick(): void {
    if (this._isDeleteEnabled()) {
      this._editor.destroyEdge(this._edge);
    }
  }

  protected _isDeleteEnabled(): boolean {
    return this._editor.toolState.isDeleteEnabled;
  }
}
