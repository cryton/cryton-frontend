import Konva from 'konva';
import { Subject } from 'rxjs';
import { Alert } from '../../../../shared/models/interfaces/alert.interface';
import { TriggerType } from '../../models/enums/trigger-type';
import { KonvaWrapper } from '../konva/konva-wrapper';
import { TemplateTimeline } from '../timeline/template-timeline';
import { TimelineNode } from '../timeline/timeline-node';
import { DeltaDependency, DeltaDependencyFinder } from '../utils/delta-dependency-finder';
import { DependencyGraph } from './dependency-graph';
import { CANVAS_PADDING } from './dependency-graph-constants';
import { EditorStageEdgeEventConfigurer } from './edge/editor-stage-edge-event-configurer';
import { EditorStepEdgeConfigurer } from './edge/editor-step-edge-configurer';
import { GraphEdge } from './edge/graph-edge';
import { RegularEdgeEventConfigurer } from './edge/regular-edge-event-configurer';
import { RegularKonvaGraphEdge } from './edge/regular-konva-graph-edge';
import { StageEdge } from './edge/stage-edge';
import { StepEdge } from './edge/step-edge';
import { KonvaGraphNode } from './konva-graph-node';
import { SettingsButton } from './node/buttons/settings-button';
import { EditorNodeEventConfigurer } from './node/editor-node-configurer';
import { GraphNode } from './node/graph-node';
import { RegularKonvaGraphNode } from './node/regular-konva-graph-node';
import { StageNode } from './node/stage-node';
import { StepNode } from './node/step-node';
import { ToolState } from './tool-state';

export class DependencyGraphEditor extends KonvaWrapper {
  alert$ = new Subject<Alert>();
  editStepEdge$ = new Subject<StepEdge>();

  graphLayerUpdate$ = new Subject<Konva.Layer>();

  depGraph: DependencyGraph;

  // STATE
  toolState = new ToolState();

  // EDGE DATA
  draggedEdge: GraphEdge; // Reference to the dragged edge.
  clickedNode: GraphNode; // Group where the arrow drag started.

  graphLayer = new Konva.Layer();

  private _nodeConfigurer = new EditorNodeEventConfigurer(this);

  constructor(private _timeline: TemplateTimeline) {
    super();
    this.depGraph = new DependencyGraph(this.theme);
  }

  changeDepGraph(depGraph: DependencyGraph): void {
    this._clearState();
    this.depGraph = depGraph;
    depGraph.wrapper = this;

    depGraph.nodes.forEach(node => {
      node.configureEvents(this._nodeConfigurer);
      node.childEdges.forEach(edge => this._configureEdge(edge));
      (node.konvaGraphNode as RegularKonvaGraphNode).setButton(this._createSettingsBtn(node));
    });

    if (depGraph.getContainer()) {
      this.graphLayer = depGraph.getContainer() as Konva.Layer;

      if (this.stage) {
        this.stage.removeChildren();
        this.stage.add(this.graphLayer);
      }
    } else {
      this.graphLayer.removeChildren();
      depGraph.draw(this.graphLayer, false);
    }
  }

  /**
   * Frees resources taken up by this object.
   */
  destroy(): void {
    super.destroy();
    this.alert$.complete();
    this.graphLayerUpdate$.complete();
  }

  /**
   * Adds node to the dependency graph.
   *
   * @param node Node to add.
   */
  addNode(node: GraphNode): void {
    const konvaGraphNode = this._createKonvaGraphNode(node);
    node.konvaGraphNode = konvaGraphNode;
    this._configureNode(node);
    this._moveNodeToInitialPosition(node);

    this.depGraph.addNode(node);
    node.addTo(this.graphLayer);

    if (node instanceof StageNode && node.trigger.getType() === TriggerType.DELTA) {
      this._timeline.addNode(new TimelineNode(node, this._timeline, this.theme));
    }
  }

  swapNode(node: GraphNode): void {
    this.destroyNodesEdges(node);
    node.unattach();
    this.depGraph.moveToDispenser(node);
    this._destroyTimelineNode(node);
  }

  deleteNode(node: GraphNode): void {
    this.destroyNodesEdges(node);
    node.destroy();
    this.depGraph.removeNode(node);
    this._destroyTimelineNode(node);
  }

  /**
   * Creates a dragged edge with a starting point at the parent node.
   *
   * @param parentNode Parent node of the edge.
   * @returns Dragged edge.
   */
  createDraggedEdge(parentNode: GraphNode): GraphEdge {
    if (parentNode instanceof StageNode) {
      this.draggedEdge = new StageEdge(parentNode);
    } else {
      this.draggedEdge = new StepEdge(parentNode);
    }

    const konvaParentNode = parentNode.konvaGraphNode;
    const initialPosition = {
      x: konvaParentNode.x + konvaParentNode.getWidth() / 2,
      y: konvaParentNode.y + konvaParentNode.getHeight()
    };

    this.draggedEdge.konvaGraphEdge = new RegularKonvaGraphEdge(this.theme, initialPosition);
    this.draggedEdge.addTo(this.graphLayer);
    this.draggedEdge.konvaGraphEdge.getKonvaObject().moveToBottom();

    return this.draggedEdge;
  }

  /**
   * Clones the current layer and emits layer update.
   */
  emitLayerUpdate(): void {
    const layerClone = this.graphLayer.clone() as Konva.Layer;
    this.graphLayerUpdate$.next(layerClone);
  }

  /**
   * Connects dragged edge to the child node.
   *
   * @param childNode GraphNode where edge ends.
   */
  connectDraggedEdge(childNode: GraphNode): void {
    if (this.draggedEdge instanceof StageEdge) {
      this._connectStageEdge(this.draggedEdge, childNode as StageNode);
    } else {
      this._connectStepEdge(this.draggedEdge as StepEdge, childNode as StepNode);
    }
  }

  destroyNodesEdges(node: GraphNode): void {
    node.childEdges.forEach(edge => this.destroyEdge(edge));
    node.parentEdges.forEach(edge => this.destroyEdge(edge));
  }

  destroyEdge(edge: GraphEdge): void {
    if (edge instanceof StageEdge) {
      this._destroyStageEdge(edge);
    } else {
      edge.destroy();
    }
  }

  private _destroyTimelineNode(node: GraphNode): void {
    const timelineNode = this._timeline.findNodeByGraphNode(node);

    if (timelineNode) {
      timelineNode.destroy();
      this._timeline.removeNode(timelineNode);
    }
  }

  /**
   * Moves node to top left corner. (initial position)
   */
  private _moveNodeToInitialPosition({ konvaGraphNode }: GraphNode) {
    konvaGraphNode.x = CANVAS_PADDING - this.stageX ?? 0;
    konvaGraphNode.y = CANVAS_PADDING - this.stageY ?? 0;
  }

  private _destroyStageEdge(edge: StageEdge): void {
    if (!edge.childNode) {
      return edge.destroy();
    }

    let childDeltas: StageNode[];

    if (edge.childNode.trigger.getType() === TriggerType.DELTA) {
      childDeltas = [edge.childNode];
    } else {
      childDeltas = DeltaDependencyFinder.getChildren(edge.parentNode);
    }
    let dependenciesBefore: DeltaDependency[];

    if (childDeltas.length > 0) {
      dependenciesBefore = DeltaDependencyFinder.getParentDependenciesOfStages(childDeltas);
    } else {
      return edge.destroy();
    }

    edge.destroy();

    const dependenciesAfter = DeltaDependencyFinder.getParentDependenciesOfStages(childDeltas);
    const removedDependencies = DeltaDependencyFinder.filterRemovedDependencies(dependenciesBefore, dependenciesAfter);

    removedDependencies.forEach((dependency: DeltaDependency) => {
      const parentTimelineNode = this._timeline.findNodeByGraphNode(dependency.parent);
      const childTimelineNode = this._timeline.findNodeByGraphNode(dependency.child);

      this._timeline.removeEdge(parentTimelineNode, childTimelineNode);
    });
  }

  private _connectStageEdge(edge: StageEdge, childNode: StageNode): void {
    let childDeltas: StageNode[];

    if (childNode.trigger.getType() === TriggerType.DELTA) {
      childDeltas = [childNode];
    } else {
      childDeltas = DeltaDependencyFinder.getChildren(childNode);
    }
    const dependenciesBefore = DeltaDependencyFinder.getParentDependenciesOfStages(childDeltas);

    try {
      edge.connect(childNode);
    } catch (error) {
      throw error;
    }

    this._configureEdge(edge);
    edge.konvaGraphEdge.getKonvaObject().moveToBottom();
    this.draggedEdge = null;

    const dependenciesAfter = DeltaDependencyFinder.getParentDependenciesOfStages(childDeltas);
    const addedDependencies = DeltaDependencyFinder.filterAddedDependencies(dependenciesBefore, dependenciesAfter);

    addedDependencies.forEach((dependency: DeltaDependency) => {
      const parentTimelineNode = this._timeline.findNodeByGraphNode(dependency.parent);
      const childTimelineNode = this._timeline.findNodeByGraphNode(dependency.child);
      this._timeline.createEdge(parentTimelineNode, childTimelineNode);
    });
  }

  private _connectStepEdge(edge: StepEdge, childNode: StepNode): void {
    try {
      edge.connect(childNode);
    } catch (e) {
      if (e instanceof Error) {
        this.alert$.next({ message: e.message, type: 'error' });
      }
      throw e;
    }

    this._configureEdge(edge);
    edge.konvaGraphEdge.getKonvaObject().moveToBottom();
    this.draggedEdge = null;

    if (edge.conditions.length === 0) {
      this.editStepEdge$.next(edge);
    }
  }

  /**
   * Refreshes dependency graph color theme.
   */
  protected _refreshTheme(): void {
    this.depGraph.updateTheme(this.theme);
  }

  /**
   * Initializes stage container and dimensions.
   *
   * @param container Container element.
   */
  protected _createStage(container: HTMLDivElement): void {
    const { width, height } = this._getBoundingRect(container);

    this.stage = new Konva.Stage({
      container,
      width,
      height,
      x: this._stageX,
      y: this._stageY,
      scale: { x: this._scale, y: this._scale },
      draggable: true
    });

    this.stage.add(this.graphLayer);
    this._initKonvaEvents();
  }

  private _configureNode(node: GraphNode): void {
    node.configureEvents(this._nodeConfigurer);
  }

  private _configureEdge(edge: GraphEdge): void {
    let edgeConfigurer: RegularEdgeEventConfigurer;

    if (edge instanceof StepEdge) {
      edgeConfigurer = new EditorStepEdgeConfigurer(this, edge, edge.konvaGraphEdge as RegularKonvaGraphEdge);
    } else {
      edgeConfigurer = new EditorStageEdgeEventConfigurer(this, edge, edge.konvaGraphEdge as RegularKonvaGraphEdge);
    }

    edge.konvaGraphEdge.configureEvents(edgeConfigurer);
  }

  private _createKonvaGraphNode(node: GraphNode): KonvaGraphNode {
    const settingsBtn = this._createSettingsBtn(node);
    return new RegularKonvaGraphNode(this.theme, node.name, node.note, settingsBtn);
  }

  private _createSettingsBtn(node: GraphNode): SettingsButton {
    return new SettingsButton(this.cursorState, () => this.depGraph.editNode(node));
  }

  private _clearState(): void {
    this.clickedNode = null;
    this.draggedEdge = null;
    this.toolState.reset();
  }

  /**
   * Initializes konva stage events.
   */
  private _initKonvaEvents(): void {
    this.stage.on('mousemove', event => {
      if (this.draggedEdge) {
        const scale = this.stage.scaleX();
        const x = (event.evt.offsetX - this.stage.x()) * (1 / scale);
        const y = (event.evt.offsetY - this.stage.y()) * (1 / scale);
        this.draggedEdge.konvaGraphEdge.end = { x, y };
      }
    });

    this.stage.on('click', e => {
      if (e.target === this.draggedEdge?.konvaGraphEdge?.getKonvaObject()) {
        this._destroyDraggedEdge();
      }
    });
  }

  /**
   * Destroys dragged arrow and resets the variables.
   */
  private _destroyDraggedEdge(): void {
    if (this.draggedEdge) {
      this.draggedEdge.destroy();
      this.draggedEdge = null;
    }
  }
}
