import Konva from 'konva';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { Queue } from 'src/app/shared/utils/queue';
import { NodeType } from '../../models/enums/node-type';
import { Bounds } from '../../models/interfaces/bounds';
import { Theme } from '../../models/interfaces/theme';
import { KonvaWrapper } from '../konva/konva-wrapper';
import { NodeOrganizer } from '../utils/node-organizer';
import { CursorState } from './cursor-state';
import { GraphEdge } from './edge/graph-edge';
import { RegularKonvaGraphEdge } from './edge/regular-konva-graph-edge';
import { StageEdge } from './edge/stage-edge';
import { StepEdge } from './edge/step-edge';
import { NodeNameNotUniqueError } from './errors/node-name-not-unique.error';
import { GraphNode } from './node/graph-node';
import { StageNode } from './node/stage-node';
import { StepNode } from './node/step-node';

export class DependencyGraph {
  nodeType: NodeType;
  wrapper: KonvaWrapper;

  /**
   * Triggers when a node is edited moved to editor.
   */
  editNode$: Observable<GraphNode>;
  moveToDispenser$: Observable<GraphNode>;
  nodes: GraphNode[] = [];

  private _container: Konva.Container;
  private _editNode$ = new ReplaySubject<GraphNode>(1);
  private _moveToDispenser$ = new Subject<GraphNode>();

  constructor(private _theme: Theme) {
    this._editNode$.next(null);
    this.editNode$ = this._editNode$.asObservable();
    this.moveToDispenser$ = this._moveToDispenser$.asObservable();
  }

  get cursorState(): CursorState {
    return this.wrapper.cursorState;
  }

  getContainer(): Konva.Container {
    return this._container;
  }

  draw(container: Konva.Container, organize: boolean): void {
    this._container = container;
    this.nodes.forEach(node => node.konvaGraphNode.draw(container));
    this.nodes.forEach(node =>
      node.childEdges.forEach(edge => {
        edge.konvaGraphEdge.draw(container);
        edge.konvaGraphEdge.getKonvaObject().moveToBottom();
      })
    );

    if (organize) {
      const nodeOrganizer = new NodeOrganizer('vertical');
      nodeOrganizer.organizeNodes(this.nodes);
    }
  }

  /**
   * Emits empty value from editNode$, prevents emitting old values.
   */
  clearEditNode(): void {
    this._editNode$.next(null);
  }

  /**
   * Moves a node from the canvas to the dispenser.
   *
   * @param node Node to move.
   */
  moveToDispenser(node: GraphNode): void {
    this.removeNode(node);
    this._moveToDispenser$.next(node);
  }

  /**
   * Emits edit node event.
   *
   * @param node Node to edit.
   */
  editNode(node: GraphNode): void {
    this._editNode$.next(node);
  }

  /**
   * Removes node from the node manager.
   *
   * @param node Node to remove.
   */
  removeNode(node: GraphNode): void {
    this.nodes = this.nodes.filter(s => s !== node);
  }

  /**
   * Adds node to the dependency graph.
   *
   * @param node Node to add.
   */
  addNode(nodeToAdd: GraphNode): void {
    if (this.nodes.map(node => node.name).includes(nodeToAdd.name)) {
      throw new NodeNameNotUniqueError(nodeToAdd.name);
    }

    this.nodes.push(nodeToAdd);
  }

  addEdge(parentNode: GraphNode, childNode: GraphNode): GraphEdge {
    let edge: GraphEdge;

    if (this.nodeType === NodeType.CRYTON_STAGE) {
      edge = new StageEdge(parentNode as StageNode);
    } else {
      edge = new StepEdge(parentNode as StepNode);
    }

    edge.konvaGraphEdge = new RegularKonvaGraphEdge(this._theme, parentNode.konvaGraphNode.getChildEdgePoint());
    edge.connect(childNode);

    return edge;
  }

  /**
   * Checks if node name is unique.
   *
   * @param name Node name.
   * @returns True if node name is unique.
   */
  isNodeNameUnique(name: string): boolean {
    return this.nodes.some(node => node.name === name);
  }

  /**
   * Makes a copy of the entire dependency graph.
   *
   * @returns Copied dependency graph.
   */
  copy(): DependencyGraph {
    const initialNodes = this.findInitialNodes();
    const graphCopy = new DependencyGraph(this._theme);

    if (initialNodes.length === 0) {
      return graphCopy;
    }

    initialNodes.forEach(initialNode => {
      this._copyGraphSection(initialNode, graphCopy);
    });

    return graphCopy;
  }

  /**
   * Checks if the dependency graph is valid.
   *
   * @returns True if dependency graph is valid.
   */
  isValid(): boolean {
    const nodeCount = this.nodes.length;

    if (nodeCount === 0) {
      return false;
    }

    return true;
  }

  /**
   * Updates theme colors inside dependency graph.
   *
   * @param theme New color theme.
   */
  updateTheme(theme: Theme): void {
    this._theme = theme;

    this.nodes.forEach(node => {
      node.konvaGraphNode.setTheme(theme);
    });
    this._getAllEdges().forEach(edge => {
      edge.konvaGraphEdge.setTheme(theme);
    });
  }

  /**
   * Finds all initial nodes in the dependency graph.
   *
   * @returns Array of initial nodes.
   */
  findInitialNodes(): GraphNode[] {
    return this.nodes.filter(node => node.parentEdges.length === 0);
  }

  /**
   * Returns an array of active errors.
   *
   * @returns Array of error messages.
   */
  errors(): string[] {
    const errors: string[] = [];
    const nodeCount = this.nodes.length;

    if (nodeCount === 0) {
      errors.push('Dependency graph is empty.');
    }

    return errors;
  }

  /**
   * Returns coordinate bounds of the whole dependency graph.
   */
  getGraphBounds(): Bounds {
    if (this.nodes.length === 0) {
      return { left: 0, right: 0, top: 0, bottom: 0 };
    }

    return {
      left: Math.min(...this.nodes.map(node => node.konvaGraphNode.x)),
      right: Math.max(...this.nodes.map(node => node.konvaGraphNode.x + node.konvaGraphNode.getWidth())),
      top: Math.min(...this.nodes.map(node => node.konvaGraphNode.y)),
      bottom: Math.max(...this.nodes.map(node => node.konvaGraphNode.y + node.konvaGraphNode.getHeight()))
    };
  }

  private _copyGraphSection(initialNode: GraphNode, graphCopy: DependencyGraph): void {
    const queue = new Queue<GraphNode>();
    queue.enqueue(initialNode);
    const copyMap: Record<string, GraphNode> = {};

    this._copyNode(initialNode, graphCopy, copyMap);

    while (queue.length > 0) {
      const currentNode = queue.dequeue();

      currentNode.childEdges.forEach((edge: GraphEdge) => {
        const childNode = edge.childNode;
        let nodeCopy = copyMap[childNode.name];

        if (!nodeCopy) {
          queue.enqueue(edge.childNode);
          nodeCopy = this._copyNode(childNode, graphCopy, copyMap);
        }

        const edgeCopy = graphCopy.addEdge(copyMap[edge.parentNode.name], nodeCopy);

        if (graphCopy.nodeType === NodeType.CRYTON_STEP) {
          (edgeCopy as StepEdge).conditions = (edge as StepEdge).conditions;
        }
      });
    }
  }

  /**
   * Returns all edges inside the dependency graph.
   */
  private _getAllEdges(): GraphEdge[] {
    const edges: GraphEdge[] = [];
    const nodes = this.nodes;

    nodes.forEach(node => edges.push(...node.childEdges));
    return edges;
  }

  /**
   * Makes a copy of a node.
   *
   * @param node Node to copy.
   * @param graphCopy New dependecy graph.
   * @param copyMap Record with node names and their copies.
   * @returns Copied node.
   */
  private _copyNode(node: GraphNode, graphCopy: DependencyGraph, copyMap: Record<string, GraphNode>): GraphNode {
    const nodeCopy = node.copy();

    copyMap[node.name] = nodeCopy;
    graphCopy.addNode(nodeCopy);

    return nodeCopy;
  }
}
