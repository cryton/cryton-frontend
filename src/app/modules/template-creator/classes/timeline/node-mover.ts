import { Vector2d } from 'konva/lib/types';
import { TICK_WIDTH } from 'src/app/shared/classes/timeline-constants';
import { TimelineUtils } from 'src/app/shared/classes/timeline-utils';
import { NodeOrganizer } from '../utils/node-organizer';
import { TemplateTimeline } from './template-timeline';
import { TimelineNode } from './timeline-node';

export class NodeMover {
  private _timeline: TemplateTimeline;
  private _nodeOrganizer = new NodeOrganizer('horizontal');

  constructor(timeline: TemplateTimeline) {
    this._timeline = timeline;
  }

  /**
   * Node drag function for the template timeline node.
   *
   * @param draggedNode Dragged template timeline node.
   * @param pos Position of the node.
   * @returns New position of the node.
   */
  nodeDragFunc = (draggedNode: TimelineNode, pos: Vector2d): Vector2d => {
    if (this._timeline.toolState.isVerticalMoveEnabled) {
      return this._verticalDragFunc(draggedNode, pos);
    } else {
      return this._horizontalDragFunc(draggedNode, pos);
    }
  };

  /**
   * Node drag function for vertical dragging.
   *
   * @param draggedNode Dragged template timeline node.
   * @param pos Position of the node.
   * @returns New position of the node.
   */
  private _verticalDragFunc = (draggedNode: TimelineNode, pos: Vector2d): Vector2d => {
    const konvaNode = draggedNode.konvaGraphNode.getKonvaObject();
    const yIncrement = pos.y - konvaNode.absolutePosition().y;

    if (this._timeline.toolState.isGraphMoveEnabled) {
      this._nodeOrganizer.moveGraph(draggedNode, 0, yIncrement);
    } else if (this._timeline.selectedNodes.size > 0) {
      this._moveSelectedNodes(draggedNode, 0, yIncrement);
    }
    return {
      x: konvaNode.absolutePosition().x,
      y: pos.y
    };
  };

  /**
   * Node drag function for horizontal dragging.
   *
   * @param draggedNode Dragged template timeline node.
   * @param pos Position of the node.
   * @returns New position of the node.
   */
  private _horizontalDragFunc = (draggedNode: TimelineNode, pos: Vector2d): Vector2d => {
    const konvaNode = draggedNode.konvaGraphNode.getKonvaObject();
    const modTickWidth = (pos.x - draggedNode.timeline.timelinePadding[3]) % TICK_WIDTH;
    const stageXModTickWidth = this._timeline.stageX % TICK_WIDTH;

    // Calculate X coordinate of closest tick to mouse position.
    let newX = modTickWidth < Math.floor(TICK_WIDTH / 2) ? pos.x - modTickWidth : pos.x + (TICK_WIDTH - modTickWidth);

    // Node gets moved away to the side by this value from the tick when stageX > 0.
    newX += stageXModTickWidth;

    const oldX = konvaNode.absolutePosition().x;

    if (newX - this._timeline.stageX < draggedNode.timeline.timelinePadding[3]) {
      newX = draggedNode.timeline.timelinePadding[3];
    }

    const xIncrement = newX - oldX;

    if (xIncrement !== 0) {
      this._moveHorizontally(draggedNode, xIncrement);
    }

    this._updateStartTime(draggedNode);

    return {
      x: newX,
      y: konvaNode.absolutePosition().y
    };
  };

  private _updateStartTime(node: TimelineNode): void {
    // We care about the position on the stage, not on the layer.
    const newSeconds = TimelineUtils.calcSecondsFromX(
      node.konvaGraphNode.x - this._timeline.stageX,
      this._timeline.getParams()
    );
    node.trigger.setStartTime(newSeconds);
  }

  /**
   * Method which is called when the node is moved horizontally in the drag bound function.
   * Makes sure that all of the dependent nodes are updated as well.
   *
   * @param draggedNode Dragged template timeline node.
   * @param xIncrement X increment.
   */
  private _moveHorizontally(draggedNode: TimelineNode, xIncrement: number): void {
    if (xIncrement === 0) {
      return;
    }
    if (this._timeline.toolState.isGraphMoveEnabled) {
      this._nodeOrganizer.moveGraph(draggedNode, xIncrement, 0);
      draggedNode.forSubgraph(node => this._updateStartTime(node as TimelineNode));
    } else if (draggedNode.selected && this._timeline.selectedNodes.size > 0) {
      this._moveSelectedNodes(draggedNode, xIncrement, 0);
      this._timeline.selectedNodes.forEach(node => this._updateStartTime(node as TimelineNode));
    }
  }

  /**
   * Moves all of the selected nodes by a given increment.
   *
   * @param draggedNode Dragged template timeline node.
   * @param xIncrement X increment.
   * @param yIncrement Y increment.
   */
  private _moveSelectedNodes(draggedNode: TimelineNode, xIncrement: number, yIncrement: number): void {
    this._timeline.selectedNodes.forEach(node => {
      const konvaGraphNode = node.konvaGraphNode;

      if (xIncrement && node !== draggedNode && konvaGraphNode.x + xIncrement >= this._timeline.timelinePadding[3]) {
        konvaGraphNode.x += xIncrement;
      }
      if (yIncrement && node !== draggedNode) {
        konvaGraphNode.y += yIncrement;
      }

      node.childEdges.forEach(edge => edge.konvaGraphEdge.refreshTracking());
      node.parentEdges.forEach(edge => edge.konvaGraphEdge.refreshTracking());
    });
  }
}
