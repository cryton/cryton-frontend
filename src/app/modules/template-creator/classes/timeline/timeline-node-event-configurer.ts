import Konva from 'konva';
import { EventConfigurer } from '../dependency-graph/event-configurer';
import { TimelineNode } from './timeline-node';

export interface TimelineNodeEventConfigurer extends EventConfigurer {
  configure(node: TimelineNode, nodeCircle: Konva.Circle, primaryLabel: Konva.Label): void;

  removeOwnListeners(node: TimelineNode, nodeCircle: Konva.Circle, primaryLabel: Konva.Label): void;

  removeAllListeners(node: TimelineNode, nodeCircle: Konva.Circle, primaryLabel: Konva.Label): void;
}
