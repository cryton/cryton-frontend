import { GraphEdge } from '../dependency-graph/edge/graph-edge';
import { TemplateTimeline } from './template-timeline';
import { TimelineNode } from './timeline-node';

export class TimelineEdge extends GraphEdge {
  constructor(public timeline: TemplateTimeline, public parentNode: TimelineNode, public childNode: TimelineNode) {
    super(parentNode);
    childNode.addParentEdge(this);
  }
}
