import { FormGroup } from '@angular/forms';
import { ImmediateTriggerParametersComponent } from '../../../components/trigger-parameters/immediate-trigger-parameters/immediate-trigger-parameters.component';
import { ImmediateArgs } from '../../../models/interfaces/immediate-args';
import { StageNode } from '../../dependency-graph/node/stage-node';
import { FormUtils } from './form-utils';
import { TriggerForm } from './trigger-form.interface';

export class ImmediateForm implements TriggerForm {
  formComponent = ImmediateTriggerParametersComponent;

  private _triggerArgsForm = new FormGroup({});

  constructor() {}

  getArgsForm(): FormGroup {
    return this._triggerArgsForm;
  }

  getArgs(): ImmediateArgs {
    return {};
  }

  isValid(): boolean {
    return this._triggerArgsForm.valid;
  }

  erase(): void {
    this._triggerArgsForm.reset();
  }

  fill(stage: StageNode): void {
    this._triggerArgsForm.setValue(stage.trigger.getArgs() as ImmediateArgs);
  }

  copy(): ImmediateForm {
    const copyForm = new ImmediateForm();
    copyForm._triggerArgsForm.setValue(this._triggerArgsForm.value as ImmediateArgs);
    return copyForm;
  }

  markAsUntouched(): void {
    this._triggerArgsForm.markAsUntouched();
  }

  isNotEmpty(): boolean {
    return FormUtils.someValueOtherThan(this._triggerArgsForm.value as Record<string, string>, 0);
  }
}
