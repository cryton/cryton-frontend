import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { withoutUndefinedAndNull } from 'src/app/shared/utils/without-undefined';
import { MsfTriggerParametersComponent } from '../../../components/trigger-parameters/msf-trigger-parameters/msf-trigger-parameters.component';
import { MSFArgs } from '../../../models/interfaces/msf-args';
import { StageNode } from '../../dependency-graph/node/stage-node';
import { FormUtils } from './form-utils';
import { TriggerForm } from './trigger-form.interface';

type MSFFormType = FormGroup<{
  module_name: FormControl<string>;
  timeout: FormControl<number>;
  datastore: FormArray<FormGroup<{ key: FormControl<string>; value: FormControl<string> }>>;
  commands: FormArray<FormGroup<{ command: FormControl<string> }>>;
}>;

export class MSFForm implements TriggerForm {
  formComponent = MsfTriggerParametersComponent;

  private _triggerArgsForm = new FormGroup({
    module_name: new FormControl<string | null>(null, []),
    timeout: new FormControl<number | null>(null, []),
    datastore: new FormArray<FormGroup<{ key: FormControl<string>; value: FormControl<string> }>>([]),
    commands: new FormArray<FormGroup<{ command: FormControl<string> }>>([])
  });

  getArgsForm(): MSFFormType {
    return this._triggerArgsForm;
  }

  getArgs(): MSFArgs {
    const { module_name, timeout, datastore, commands } = this._triggerArgsForm.value;

    const args: MSFArgs = {
      module_name: module_name,
      timeout: timeout,
      datastore: {},
      commands: commands.map(command => command.command)
    };
    datastore.forEach(variable => (args.datastore[variable.key] = variable.value));

    return withoutUndefinedAndNull(args) as MSFArgs;
  }

  isValid(): boolean {
    const moduleName = this._triggerArgsForm.controls.module_name.value;
    const commands = this._triggerArgsForm.controls.commands.value;
    return (
      this._triggerArgsForm.valid &&
      !(moduleName !== null && moduleName !== '' && commands.length > 0) &&
      ((moduleName !== null && moduleName !== '') || commands.length > 0)
    );
  }

  erase(): void {
    this._triggerArgsForm.reset();
    const identifiers = this._triggerArgsForm.controls.datastore;
    identifiers.clear();
  }

  fill(stage: StageNode): void {
    this._fillMSFForm(this, stage.trigger.getArgs() as MSFArgs);
  }

  copy(): MSFForm {
    const copyForm = new MSFForm();
    this._fillMSFForm(copyForm, this.getArgs());
    return copyForm;
  }

  markAsUntouched(): void {
    this._triggerArgsForm.markAsUntouched();
  }

  isNotEmpty(): boolean {
    return FormUtils.someValueOtherThan(this._triggerArgsForm.value, null);
  }

  addDatastore(key: string = null, value: string = null): void {
    const datastore = this._triggerArgsForm.controls.datastore;
    datastore.insert(
      0,
      new FormGroup({
        key: new FormControl(key, [Validators.required]),
        value: new FormControl(value, [Validators.required])
      })
    );
  }

  removeDatastore(index: number): void {
    const datastore = this._triggerArgsForm.controls.datastore;
    datastore.removeAt(index);
  }

  addCommand(command: string = null): void {
    this._triggerArgsForm.controls.commands.insert(
      0,
      new FormGroup({
        command: new FormControl(command, [Validators.required])
      })
    );
  }

  removeCommand(index: number): void {
    this._triggerArgsForm.controls.commands.removeAt(index);
  }

  private _fillMSFForm(form: MSFForm, args: MSFArgs): void {
    const { datastore, commands, ...rest } = args;

    if (datastore) {
      Object.entries(datastore).forEach(([key, value]) => form.addDatastore(key, value));
    }

    if (commands) {
      commands.forEach(command => form.addCommand(command));
    }

    form._triggerArgsForm.patchValue({
      ...rest
    });
  }
}
