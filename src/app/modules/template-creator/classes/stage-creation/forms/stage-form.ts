import { Type } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { stringify } from 'yaml';
import { TriggerType } from '../../../models/enums/trigger-type';
import { StageNode } from '../../dependency-graph/node/stage-node';
import { TriggerArgs } from '../../triggers/trigger';
import { yamlValidator } from '../../utils/validate-yaml';
import { TriggerParameters } from '../trigger-parameters';
import { DateTimeForm } from './date-time-form';
import { DeltaForm } from './delta-form';
import { HttpForm, HttpTriggerForm } from './http-form';
import { ImmediateForm } from './immediate-form';
import { MSFForm } from './msf-trigger-form';
import { TriggerForm } from './trigger-form.interface';

const INITIAL_TRIGGER = TriggerType.IMMEDIATE;

export interface StageArgs {
  name: string;
  metadata: string;
  triggerType: TriggerType;
}

export class StageForm {
  editedNodeName = null;
  triggerTypeChange$: Observable<TriggerType>;

  private _stageArgsForm: FormGroup;
  private _triggerForm: TriggerForm;
  private _triggerBackup: Record<string, TriggerForm> = {};

  constructor() {
    this._stageArgsForm = new FormGroup({
      name: new FormControl<string | null>(null, [Validators.required]),
      metadata: new FormControl<string>(null, {
        validators: [yamlValidator()],
        updateOn: 'blur'
      }),
      triggerType: new FormControl(INITIAL_TRIGGER, Validators.required)
    });
    this._triggerForm = this._createTriggerForm(INITIAL_TRIGGER);

    this.triggerTypeChange$ = this._stageArgsForm.get('triggerType').valueChanges as Observable<TriggerType>;
    this.triggerTypeChange$.subscribe(value => this._changeTriggerForm(value));
  }

  getStageArgsForm(): FormGroup {
    return this._stageArgsForm;
  }

  getStageArgs(): StageArgs {
    return this._stageArgsForm.value;
  }

  getTriggerForm(): TriggerForm {
    return this._triggerForm;
  }

  getTriggerArgsForm(): FormGroup | HttpTriggerForm {
    return this._triggerForm.getArgsForm();
  }

  getTriggerArgs(): TriggerArgs {
    return this._triggerForm.getArgs();
  }

  getTriggerFormComponent(): Type<TriggerParameters> {
    return this._triggerForm.formComponent;
  }

  fill(stage: StageNode): void {
    this._stageArgsForm.setValue({
      name: stage.name,
      metadata: stage.metadata ? stringify(stage.metadata) : '',
      triggerType: stage.trigger.getType()
    });
    this._triggerForm.fill(stage);
  }

  fillWithEditedStage(stage: StageNode): void {
    this.editedNodeName = stage.name;
    this.fill(stage);
  }

  erase(): void {
    this._triggerForm.erase();
    this._stageArgsForm.get('name').reset();
    this._stageArgsForm.get('metadata').reset('');
    this._stageArgsForm.get('triggerType').setValue(INITIAL_TRIGGER);
  }

  cancelEditing(): void {
    this.editedNodeName = null;
    this.erase();
  }

  isValid(): boolean {
    return this._stageArgsForm.valid && this._triggerForm.isValid();
  }

  markAsUntouched(): void {
    this._stageArgsForm.markAsUntouched();
    this._triggerForm.markAsUntouched();
  }

  copy(): StageForm {
    const copyForm = new StageForm();

    copyForm._stageArgsForm.setValue(this._stageArgsForm.value);
    copyForm._triggerForm = this._triggerForm.copy();

    return copyForm;
  }

  isNotEmpty(): boolean {
    return Boolean(this._stageArgsForm.get('name').value) || this._triggerForm.isNotEmpty();
  }

  private _changeTriggerForm(type: TriggerType): void {
    const backupForm = this._triggerBackup[type];

    // Do not load backup if we are editing a stage.
    if (!this.editedNodeName && backupForm) {
      this._triggerForm = backupForm;
    } else {
      this._triggerForm = this._createTriggerForm(type);
    }
  }

  private _createTriggerForm(type: TriggerType): TriggerForm {
    let triggerForm: TriggerForm;

    switch (type) {
      case TriggerType.IMMEDIATE:
        triggerForm = new ImmediateForm();
        break;
      case TriggerType.DELTA:
        triggerForm = new DeltaForm();
        break;
      case TriggerType.HTTP:
        triggerForm = new HttpForm();
        break;
      case TriggerType.TIME:
        triggerForm = new DateTimeForm();
        break;
      case TriggerType.METASPLOIT:
        triggerForm = new MSFForm();
        break;
      default:
        throw new Error('Unknown trigger type');
    }

    this._triggerBackup[type] = triggerForm;
    return triggerForm;
  }
}
