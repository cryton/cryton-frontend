import { TriggerType } from '../../models/enums/trigger-type';
import { DateTimeArgs } from '../../models/interfaces/date-time-args';
import { DeltaArgs } from '../../models/interfaces/delta-args';
import { HTTPListenerArgs } from '../../models/interfaces/http-listener-args';
import { MSFArgs } from '../../models/interfaces/msf-args';
import { DateTimeTrigger } from './date-time-trigger';
import { DeltaTrigger } from './delta-trigger';
import { HttpTrigger } from './http-trigger';
import { ImmediateTrigger } from './immediate-trigger';
import { MSFTrigger } from './msf-trigger';
import { Trigger, TriggerArgs } from './trigger';

export class TriggerFactory {
  constructor() {}

  static createTrigger(type: TriggerType, args: TriggerArgs): Trigger<TriggerArgs> {
    switch (type) {
      case TriggerType.IMMEDIATE:
        return new ImmediateTrigger(args as DeltaArgs);
      case TriggerType.DELTA:
        return new DeltaTrigger(args as DeltaArgs);
      case TriggerType.HTTP:
        return new HttpTrigger(args as HTTPListenerArgs);
      case TriggerType.TIME:
        return new DateTimeTrigger(args as DateTimeArgs);
      case TriggerType.METASPLOIT:
        return new MSFTrigger(args as MSFArgs);
      default:
        throw new Error('Unknown trigger type.');
    }
  }
}
