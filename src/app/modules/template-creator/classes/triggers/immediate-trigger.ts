import { TriggerType } from '../../models/enums/trigger-type';
import { ImmediateArgs } from '../../models/interfaces/immediate-args';
import { Trigger } from './trigger';

export class ImmediateTrigger extends Trigger<ImmediateArgs> {
  constructor(args: ImmediateArgs) {
    super(args, TriggerType.IMMEDIATE);
  }

  /**
   * Edits delta trigger arguments.
   * No argument should be left undefined.
   *
   * @param args Delta arguments.
   */
  editArgs(args: ImmediateArgs): void {
    this._args = {};
  }

  getStartTime(): number {
    return;
  }

  setStartTime = (): void => {};
}
