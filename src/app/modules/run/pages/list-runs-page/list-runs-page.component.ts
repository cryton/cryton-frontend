import { Component, OnInit, ViewChild } from '@angular/core';
import { RunService } from 'src/app/core/http/run/run.service';
import { Run } from 'src/app/models/api-responses/run.interface';
import { renderComponentTrigger } from 'src/app/shared/animations/render-component.animation';
import { LinkButton } from 'src/app/shared/components/cryton-table/buttons/link-button';
import { TableButton } from 'src/app/shared/components/cryton-table/buttons/table-button';
import { CrytonTableComponent } from 'src/app/shared/components/cryton-table/cryton-table.component';
import { RunTableDataSource } from 'src/app/shared/components/cryton-table/data-source/run-table.data-source';
import { ExpandedRunManipulationComponent } from 'src/app/shared/components/run-manipulation/expanded-run-manipulation.component';
import { CrytonDatetimePipe } from 'src/app/shared/pipes/cryton-datetime.pipe';

@Component({
  selector: 'app-list-runs-page',
  templateUrl: './list-runs-page.component.html',
  animations: [renderComponentTrigger]
})
export class ListRunsPageComponent implements OnInit {
  @ViewChild(CrytonTableComponent) runsTable: CrytonTableComponent<Run>;

  dataSource: RunTableDataSource;
  expandedComponent = ExpandedRunManipulationComponent;
  buttons: TableButton[];

  constructor(private _runService: RunService, private _crytonDatetime: CrytonDatetimePipe) {
    this.buttons = [
      new LinkButton('Show run', 'visibility', '/app/runs/:id'),
      new LinkButton('Show timeline', 'schedule', '/app/runs/:id/timeline'),
      new LinkButton('Show YAML', 'description', '/app/runs/:id/yaml')
    ];
  }

  ngOnInit(): void {
    this.dataSource = new RunTableDataSource(this._runService, this._crytonDatetime);
  }
}
