import { ClipboardModule } from '@angular/cdk/clipboard';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatIconModule } from '@angular/material/icon';
import { CodeEditorModule } from '../../code-editor.module';
import { CodePreviewComponent } from './code-preview.component';

describe('CodePreviewComponent', () => {
  let component: CodePreviewComponent;
  let fixture: ComponentFixture<CodePreviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CodePreviewComponent],
      imports: [ClipboardModule, MatIconModule, CodeEditorModule]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CodePreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
