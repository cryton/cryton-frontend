import { ClipboardModule } from '@angular/cdk/clipboard';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { CodeEditorTextareaComponent } from './components/code-editor-textarea/code-editor-textarea.component';
import { CodePreviewComponent } from './components/code-preview/code-preview.component';
import { YamlPreviewComponent } from './components/yaml-preview/yaml-preview.component';
import { YamlPipe } from './pipes/yaml.pipe';

@NgModule({
  declarations: [CodeEditorTextareaComponent, CodePreviewComponent, YamlPreviewComponent, YamlPipe],
  imports: [CommonModule, FormsModule, MatIconModule, MatButtonModule, ClipboardModule, MatTooltipModule],
  exports: [CodeEditorTextareaComponent, CodePreviewComponent, YamlPreviewComponent],
  providers: [YamlPipe]
})
export class CodeEditorModule {}
