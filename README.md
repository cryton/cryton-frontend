# Cryton Frontend
Cryton Frontend is a graphical interface used to interact with [Cryton](https://gitlab.ics.muni.cz/cryton/cryton)'s REST API.

Cryton frontend is tested and targeted primarily on **Debian** and **Kali Linux**. Please keep in mind that **only the latest version is supported** and issues regarding different OS or distributions may **not** be resolved.

For more information see the [documentation](https://cryton.gitlab-pages.ics.muni.cz/).

## Quick-start
Please, follow the guide in the [documentation](https://cryton.gitlab-pages.ics.muni.cz/latest/quick-start/).

## Development
Please, follow the guide in the [documentation](https://cryton.gitlab-pages.ics.muni.cz/latest/development/frontend/).

## How to contribute
Please, follow the guide in the [documentation](https://cryton.gitlab-pages.ics.muni.cz/latest/contribution-guide/).
