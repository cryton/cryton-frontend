FROM node:18-alpine as builder

WORKDIR /app

# Install dependencies
COPY docker-package.json ./package.json
RUN npm install --ignore-scripts --legacy-peer-deps

# Build app
COPY . .
RUN npm run build-prod

FROM nginx:1.22-alpine as production

# Replace the default configuration
COPY default.conf /etc/nginx/conf.d/default.conf

# Copy app
COPY --from=builder /app/dist/cryton-frontend /usr/share/nginx/html
